msgid ""
msgstr ""
"PO-Revision-Date: 2021-10-01 00:39+0000\n"
"Last-Translator: phlostically <phlostically@mailinator.com>\n"
"Language-Team: Esperanto <https://hosted.weblate.org/projects/"
"super-tux-party/minigameshaunted_dreams/eo/>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9-dev\n"

msgid "MINIGAME_DESCRIPTION"
msgstr "La ludantoj protektu la liton de la infano kontraŭ fantomoj."

msgid "MINIGAME_NAME"
msgstr "Hantataj Sonĝoj"

msgid "MINIGAME_ACTION_RIGHT"
msgstr "Movi sin dekstren"

msgid "MINIGAME_ACTION_LEFT"
msgstr "Movi sin maldekstren"

msgid "MINIGAME_ACTION_DOWN"
msgstr "Movi sin malsupren"

msgid "MINIGAME_ACTION_UP"
msgstr "Movi sin supren"
