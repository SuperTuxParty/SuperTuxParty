## screenshot.png

Copyright © 2018 Jakob Sinclair

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## assets/textures/controls/gamepad
### arrow*.png | button*.png | dpad*.png

Public domain by Terazilla

buttonSelect.png is a modified version of kb_space.png by Florian Kothmeier

dpad_left, dpad_right, dpad_up and dpad_down are modified versions of switch_dpad_all.png

Retrieved from [Open Game Art](https://opengameart.org/content/free-controller-prompts-xbox-ps4-switch)

License: [CC0 1.0 Universal](http://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/textures/controls/keyboard
### *.png

Public domain by Terazilla

key_blank.png is a modified version of left.png by Florian Kothmeier

Retrieved from [Open Game Art](https://opengameart.org/content/free-controller-prompts-xbox-ps4-switch)

License: [CC0 1.0 Universal](http://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/textures/controls/mouse
### *.png

Public domain by Terazilla

Retrieved from [Open Game Art](https://opengameart.org/content/free-controller-prompts-xbox-ps4-switch)

License: [CC0 1.0 Universal](http://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/models/
### block.glb

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### goal.glb | goal_small.glb | hammer.glb | hurdle.glb | *_Atlas_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/models/Bomb
### Bomb.obj
Copyright © Blender3D

Retrieved from: [Sketchfab](https://sketchfab.com/3d-models/simple-bomb-49ab3dbdfa6347dfb6e2b633c9c7f1c2)

License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

## assets/models/boat/
### boat.glb | boat_Atlas_Party.png | paddle.glb | paddle_Atlas_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/models/Bomb/materials
### \*.png
Copyright © Blender3D
Downscaled and converted to png by Florian Kothmeier

Retrieved from: [Sketchfab](https://sketchfab.com/3d-models/simple-bomb-49ab3dbdfa6347dfb6e2b633c9c7f1c2)

License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

## assets/models/bed
### bed.glb | bed.blend | bed_Atlas_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/models/bridge
### bridge_mesh.blend | bridge_mesh.obj | bridge_diffuse.png

Public domain by JamesWhite

Retrieved from [OpenGameArt](https://opengameart.org/content/wooden-bridge-0)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/models/cannon
### scene.glb | scene_Atlas_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/models/computer
### computer.glb | computer_Atlas_Party.png | laptop.glb | laptop_Atlas_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/models/door/
### WoodenDoor.blend | WoodenDoor.glb
Copyright Danimal and Yughues

Retrieved from [Open Game Art](https://opengameart.org/content/wooden-door)

License: [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/legalcode)

## assets/models/door/images
### WoodenDoorNormal.png | WoodenDoor.png

Copyright Danimal and Yughues

Retrieved from [Open Game Art](https://opengameart.org/content/wooden-door)

License: [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/legalcode)

## assets/models/finishline
### finishline.glb | finishline\_Atlas\_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/models/furniture/books
### book*.glb | book*_Atlas_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/models/furniture/
### chair.glb | chair_Atlas_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### desk.glb | desk_Atlas_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### lamp.glb | lamp_Atlas_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### organizer.glb | organizer_Atlas_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### rug.glb | rug_Atlas_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/models/ghost
### ghost.glb | ghost.blend | ghost_Atlas_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/models/food/OBJ
### Pumpkin.obj

Public Domain by Quaternius

Modified by Florian Kothmeier

Retrieved from [Quaternius.com](https://quaternius.com/packs/ultimatefood.html)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/models/nature/glTF
### \*.gltf

Public Domain by Quaternius

Retrieved from [Quaternius.com](https://quaternius.com/packs/ultimatestylizednature.html)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/models/nature/Textures
### *.png

Public Domain by Quaternius

Retrieved from [Quaternius.com](https://quaternius.com/packs/ultimatestylizednature.html)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/models/rails
### straight.glb | curve.glb | curve2.glb | *_Atlas_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## common/scenes/board_logic/controller/icons
### cake.png

Public Domain by maruki

Retrived from [Open Game Art](https://opengameart.org/content/foodies)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### cookie.png

Copyright © 2017 InkMammoth

Retrived from [Open Game Art](https://opengameart.org/content/pixel-art-food-pack-by-inkmammoth)

License: [GPL 2.0](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

### indicator.png

Copyright © 2018 Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### splash_background.png

Copyright © 2018 Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### gnu_icon.png

Copyright Néd J. Édoire

Retrieved from: [OpenGameArt](https://opengameart.org/content/gnu-mascots-and-friends-iconset)
License: [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

### nolokicon.png

Copyright Anthony Carré (yekcim)

Retrieved from: [Super Tux Kart Sourceforge](https://sourceforge.net/p/supertuxkart/code/HEAD/tree/media/trunk/karts/nolok/nolokicon.png)
License: [GPL 2.0](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

### sara.png

Copyright © Spring

Retrieved from [Open Game Art](https://opengameart.org/content/sara-sketch-portrait-improvement)

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## common/scenes/board_logic/node
### node.blend | node.glb

Copyright © 2020 Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## common/scenes/board_logic/node/arrow
### arrow.png

Copyright © 2018 Jakob Sinclair

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### arrow_keep.png

Copyright © 2018 Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## common/scenes/board_logic/node/material
### tile_blue_col.png | tile_red_col.png | tile_yellow_col.png | tile_green_col.png

Copyright © 2020 Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### tile_purple_col.png

Copyright © 2020 Florian Kothmeier

Uses "Free Any House" shop icon by strongicon

Retrieved from [iconfinder](https://www.iconfinder.com/iconsets/free-any-house)

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### tile_gnu_col.png

Based upon the gnu icon file by Néd J. Édoire

Retrieved from: [OpenGameArt](https://opengameart.org/content/gnu-mascots-and-friends-iconset)
License: [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

### tile_nolok_col.jpg

Based upon the nolok icon by Anthony Carré (yekcim)

Retrieved from: [Super Tux Kart Sourceforge](https://sourceforge.net/p/supertuxkart/code/HEAD/tree/media/trunk/karts/nolok/nolokicon.png)
License: [GPL 2.0](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## client/menus/victory_screen
### stage.glb | stage.blend

Copyright © 2020 Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## common/scenes/speech_dialog
### dialog_box.png | dialog_box_focus.png

Copyright © 2019 Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## client/team_indicator
### indicator.png

Copyright © 2019 Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## plugins/boards/KDEValley/landscape
### kdevalley.blend | kdevalley.glb

Copyright © Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## plugins/boards/KDEValley/dragons
### dragon\*_head.png | eyes.png

Copyright © 2019 Florian Kothmeier 

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### dragon\*.glb

Copyright © 2019 Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### dragon\*_icon.png

Copyright © 2019 Néd J

Modified by Florian Kothmeier 2019

Retrieved from: [OpenGameArt](https://opengameart.org/content/mascots-and-friends-iconset-2)

License: [CC BY SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

## plugins/boards/test/islands
### island1.blend | island1.dae | island2.blend | island2.dae

Copyright © Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### Sand3.jpg

Public domain by MrCraft Animation

Retrieved from [OpenGameArt](https://opengameart.org/content/sand-texture-pack)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## plugins/characters/Tux
### Atlas_Party.png | tux.blend | tux.glb

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### splash.png

Copyright © 2020 Paulius Danelius

License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

### icon.png

Copyright © 2019 Néd J

Retrieved from [Open Game Art](https://opengameart.org/content/mascots-and-friends-iconset-1)

License: [CC BY SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

## plugins/characters/Green Tux
### Atlas_Party.png

Public Domain by Quaternius
Modified by Yvonne Kothmeier

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### splash.png

Copyright © 2020 Paulius Danelius

License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

### icon.png

Copyright © 2019 Néd J

Modified by Florian Kothmeier

Retrieved from [Open Game Art](https://opengameart.org/content/mascots-and-friends-iconset-1)

License: [CC BY SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

## plugins/characters/Beastie
### beastie_tex.png | beastie.blend | beastie.glb

Copyright © 2010 durmieu

Modified by Florian Kothmeier 2018

Retrived from [Open Game Art](https://opengameart.org/content/beastie)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### splash.png

Copyright © 2020 Paulius Danelius

License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

### icon.png

Copyright © 2020 Paulius Danelius

License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

## plugins/characters/Godette
### palette.png | godot_chan.blend | godot_chan.glb

Copyright © SirRichard94

Models modified by Florian Kothmeier 2018

Retrieved from [Github](https://github.com/SirRichard94/low-poly-godette)

License: [CC BY 3.0](https://github.com/SirRichard94/low-poly-godette/blob/master/License)

### splash.png

Copyright © 2020 Paulius Danelius

License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

### icon.png

Copyright © 2020 Paulius Danelius

License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

## plugins/items/cookie_steal_trap
### icon.png | icon.xcf | material.png | material.xcf

Public domain by Vytautas Butėnas

License: [CC0](https://creativecommons.org/licenses/zero/1.0/legalcode)

## plugins/items/dice
### icon.png | icon.xcf | dice.blend

Public domain by Vytautas Butėnas

License: [CC0](https://creativecommons.org/licenses/zero/1.0/legalcode)

## plugins/items/lucky_seven
### icon.png | icon.xcf | lucky_seven.blend

Public domain by Vytautas Butėnas

License: [CC0](https://creativecommons.org/licenses/zero/1.0/legalcode)

## assets/icons/blender
### icon.blend

Copyright © 2018 Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### tux_texture.png

Copyright © 2010 durmieu

Modified by Florian Kothmeier 2018

Retrieved from [Open Game Art](https://opengameart.org/content/tux)

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### blue-sky-merge-clouds-675977.jpeg

Public Domain by Skitterphoto

Retrieved from [pexels.com](https://www.pexels.com/photo/air-atmosphere-blue-blue-sky-675977)

License: [CC0](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/icons/loading
### load*.png

Copyright © 2017 KopiteCowboy

Retrieved from [wikipedia.org](https://cs.wikipedia.org/wiki/Soubor:Loading_2.gif)

License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

## assets/icons
### icon.\* | icon-\*

Copyright © 2018 Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### heart.png | heart_grayscale.png

Copyright © [DontMind8](http://dontmind8.blogspot.com)

Retrieved from [Open Game Art](https://opengameart.org/content/heart-pixel-art)

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### cookie-grayscale.png

Copyright © 2017 InkMammoth, Modified by Jakob Sinclair

Retrieved from [Open Game Art](https://opengameart.org/content/pixel-art-food-pack-by-inkmammoth)

License: [GPL 2.0](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## plugins/minigames/memory
### screenshot.png

Copyright © Florian Kothmeier

License: [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

### montage.ogg

Public Domain by wipics

Retrieved from [Open Game Art](https://opengameart.org/content/montage)

License: [CC0](http://creativecommons.org/publicdomain/zero/1.0/legalcode)

### 84322__splashdust__flipcard.wav

Public Domain by Splashdust

Retrieved from [FreeSound](https://freesound.org/people/Splashdust/sounds/84322/)

License: [CC0](http://creativecommons.org/publicdomain/zero/1.0/legalcode)

## plugins/minigames/memory/cards
### card_\*.png

Public Domain by mehrasaur

Retrieved from [Open Game Art](https://opengameart.org/content/playing-card-assets-52-cards-deck-chips)

License: [CC0](http://creativecommons.org/publicdomain/zero/1.0/legalcode)

## plugins/minigames/bowling
### ball.glb | ball_Atlas_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### screenshot.png

Copyright © Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## plugins/minigames/bowling/room
### room.blend | room.glb
Copyright © Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## plugins/minigames/harvest_food
### carrot.png

Public Domain by thekingphoenix

Retrieved from [Open Game Art](https://opengameart.org/content/icons-food)

License: [CC0](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### screenshot.png

Copyright © Florian Kothmeier

License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

## plugins/minigames/haunted_dreams
### room.glb | room.blend

Copyright © Vytautas Butėnas, Florian Kothmeier

License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

### floor.glb | floor_bed_Atlas_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### penholder.glb | penholder_Atlas_Party.png

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### screenshot.png

Copyright © Florian Kothmeier

License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

## plugins/minigames/haunted_dreams/images
### Blood.png | Web.png
Public Domain by Vytautas Butėnas

License: [CC0](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## plugins/minigames/dungeon_parkour/
### screenshot.png

Copyright © Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## plugins/minigames/dungeon_parkour/Dungeon Arena
### ColorPaletteRED.png | Dungeon Arena.blend

Public Domain by AurynSky

Retrieved from: [OpenGameart](https://opengameart.org/content/dungeon-low-poly-toon-battle-arena-tower-defense-pack)

License: [CC0](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## plugins/minigames/dungeon_parkour/Dungeon Arena/exported
### \*.escn

Public Domain by AurynSky

Retrieved from: [OpenGameart](https://opengameart.org/content/dungeon-low-poly-toon-battle-arena-tower-defense-pack)

License: [CC0](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## plugins/minigames/boat_rally/
### screenshot.png

Copyright © Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## plugins/minigames/boat_rally/pirate_kit
### \*.gltf

Public Domain by Kenney

Retrieved from: [OpenGameart](https://opengameart.org/content/pirate-kit)

License: [CC0](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## plugins/minigames/forest_run
### screenshot.png

Copyright © Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## plugins/minigames/forest_run/Forest Arena
### ColorPaletteNormal.png | Forest Arena Blend.blend

Public Domain by AurynSky

Retrieved from: [OpenGameart](https://opengameart.org/content/forest-low-poly-toon-battle-arena-tower-defense-pack)

License: [CC0](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## plugins/minigames/forest_run/Forest Arena/exported
### \*.escn

Public Domain by AurynSky

Retrieved from: [OpenGameart](https://opengameart.org/content/forest-low-poly-toon-battle-arena-tower-defense-pack)

License: [CC0](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## assets/models/fence
### fence.escn | texture.png

Copyright © DeadKir
Fence model extracted by Florian Kothmeier

Retrieved from [Open Game Art](https://opengameart.org/content/farm-0)

License: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt)

## plugins/minigames/hurdle
### screenshot.png

Copyright © 2022 Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## plugins/minigames/hurdle/ground
### conveyor_belt.glb

Copyright © Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## plugins/minigames/hurdle/hurdle
### hurdle.glb

Copyright © Jakob Sinclair

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### plastic_\*

Public Domain by StruffelProductions

Retrived from [CC0 Textures](https://cc0textures.com/home)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## plugins/minigames/knock_off
### ice.glb

Public Domain by Quaternius

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### screenshot.png

Coyright © Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## assets/models/ice
### IceC_S.jpg | IceC_N.jpg

Public Domain by Keith333

Retrieved from [OpenGameArt.org](https://opengameart.org/content/snow-and-ice-batch-of-15-seamless-textures-with-normalmaps)

IceC_S.jpg modified by Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## plugins/minigames/kernel_compiling
### screenshot.png

Copyright © Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## plugins/minigames/kernel_compiling/screen
### fill.png

Copyright © Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## plugins/minigames/kernel_compiling/room
### room.blend | room.glb | room_Atlas_Party.png

Copyright © Florian Kothmeier with shelves models by Vytautas Butėnas

Book model by Quaternius

Book model license: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### book3.png

Copyright © RayB2

Retrieved from [OpenGameArt](https://opengameart.org/content/old-fashioned-book)

License: [CC0](https://creativecommons.org/licenses/zero/1.0/legalcode)

## assets/models/screen
### screen.blend | screen.escn

Copyright © Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## assets/models/tree
### tree.obj | tree_snow.obj
Public Domain by Mitylernal

Retrieved from [OpenGameArt](https://opengameart.org/content/low-poly-tree-1)

License: [CC0](https://creativecommons.org/licenses/zero/1.0/legalcode)

## assets/models/KDE Dragon
### dragon.blend

Copyright © 2019 Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### eyes.png | Head.png | Scarf.png

Copyright © 2019 Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## plugins/minigames/knock_off/player_ball_material
### paper02_col.jpg | paper02_nrm.jpg | paper02_rgh.jpg

Public Domain by StruffelProductions

Retrived from [CC0 Textures](https://cc0textures.com/home)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### SnowB_S.jpg | SnowB_N.jpg

Public Domain by Keith333

Retrieved from [OpenGameArt.org](https://opengameart.org/content/snow-and-ice-batch-of-15-seamless-textures-with-normalmaps)

SnowB_S.jpg modified by Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## plugins/minigames/escape_from_lava
### screenshot.png

Copyright © Florian Kothmeier

License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

### ground.blend | ground.dae

Copyright Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## plugins/minigames/escape_from_lava_1v3
### screenshot.png

Copyright © Florian Kothmeier

License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

### ground.blend

Copyright Florian Kothmeier

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## assets/textures
### dirt.png

Copyright © Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### water_diffuse.png

Public Domain by Bastiaan Olij

Retrived from [Github](https://github.com/BastiaanOlij/shader_tutorial)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

### lava.png
Copyright by Maujoe

Retrieved from [Github](https://github.com/Maujoe/godot-flow-map-shader)

License: [MIT](https://github.com/Maujoe/godot-flow-map-shader/blob/master/LICENSE.md)

### painted_wall.png
Public domain by XCVG

Retrieved from [OpnGameArt](https://opengameart.org/content/wall-paint-and-colors)

License: [CC0](https://creativecommons.org/licenses/zero/1.0/legalcode)

### pt_white.png
Public domain by XCVG

Retrieved from [OpnGameArt](https://opengameart.org/content/wall-paint-and-colors)

License: [CC0](https://creativecommons.org/licenses/zero/1.0/legalcode)

### fire.png
Public domain by Kenney

Retrieved from [kenney.nl](https://www.kenney.nl/assets/particle-pack)

License: [CC0](https://creativecommons.org/licenses/zero/1.0/legalcode)

### finish.png
Copyright © Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### hit_indicator.png
Copyright © Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### stun.png
Copyright © Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### metal.png
Copyright © Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

### wrong.png
Copyright © Florian Kothmeier

License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## assets/textures/crate\*
### crate\*.png
Public domain by Luke.RUSTLTD

Retrieved from [OpenGameArt](https://opengameart.org/content/3-crate-textures-w-bump-normal)

License: [CC0](https://creativecommons.org/licenses/zero/1.0/legalcode)


## assets/textures/grass
### grass_\*
Public domain by rubberduck

Retrieved from [OpenGameArt](https://opengameart.org/content/handpainted-grass-texture-pack)

License: [CC0](https://creativecommons.org/licenses/zero/1.0/legalcode)

## assets/textures/rock
### rock_05_*
Public domain by rubberduck

Retrieved from [OpenGameArt](https://opengameart.org/content/handpainted-rock-texture)

License: [CC0](https://creativecommons.org/licenses/zero/1.0/legalcode)

## assets/textures/wood_planks
### wood2_\*.jpg
Copyright © by kinnybean

Retrieved from [OpenGameArt](https://opengameart.org/content/simple-seamless-hand-painted-wooden-planks-texture)

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## assets/textures/wood
### bump.png | diffuse.png
Copyright © by cemkalyoncu and Beetree

Retrieved from [OpenGameArt](https://opengameart.org/content/wood-texture)

License: [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/legalcode)

## assets/models/cake
### circle.png | cake.blend | cake.glb
Public domain by Vytautas Butėnas

License: [CC0](https://creativecommons.org/licenses/zero/1.0/legalcode)

### tux_texture.png

Copyright © 2010 durmieu

Modified by Florian Kothmeier 2018

Retrieved from [Open Game Art](https://opengameart.org/content/tux)

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## assets/textures/title
### Title.png
Public domain by Vytautas Butėnas

License: [CC0](https://creativecommons.org/licenses/zero/1.0/legalcode)

## assets/tux
### tux.glb

Copyright © 2010 durmieu

Modified by Florian Kothmeier 2018

Retrived from [Open Game Art](https://opengameart.org/content/tux)

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## assets/icons

### delete.png

Copyright © Godot Engine

Retrieved from [github.com](https://github.com/godotengine/godot-design/blob/master/engine/icons/original/icon_remove.png)

License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

### edit.png

Copyright © Godot Engine

Retrieved from [github.com](https://github.com/godotengine/godot-design/blob/fb3480f4831204d420b0eb7b1bd78335419d299b/engine/icons/original/icon_edit.png)

License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

### off.svg | off.png

Copyright © Google
Retrieved from [Material Design](https://material.io/resources/icons/?search=power_settings_new&icon=power_settings_new&style=round)

Licensed: [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)

## assets/defaults/theme_icons
### GraberPress.svg | Graper.svg | NotSelected.svg | Off.svg | On.svg | Scroll.svg | Selected.svg

Copyright © 2020 Independent-Eye

License: [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

## plugins/minigames/memory
### background_blue.svg | background_red.svg

Copyright © 2020 Florian Kothmeier

License: [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)
